const pobj1 = new Promise((resolve, reject) => {
    setTimeout(() => {
        let roll_no = [1, 2, 3, 4]
        resolve(roll_no)
    }, 2000)
})

const getBiodata = (indexdata) => {
    return new Promise((resolve, reject) => {
        setTimeout((indexdata) => {
            let biodata = {
                name: 'yogita',
                age: '22'
            }
            resolve(`My name is ${biodata.name} and I am ${biodata.age} years old`)
        }, 2000, indexdata)
    })
}

async function getData() {
    const rollnodata = await pobj1;
    console.log(rollnodata)

    const biodata1 = await getBiodata(rollnodata[1]);
    console.log(biodata1)

    return biodata1;
}

const getname = getData();
console.log(getname)