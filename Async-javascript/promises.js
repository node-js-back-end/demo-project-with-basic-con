const userOneMeme= false
const userTwoMeme= false

function watchUserPromise(){
    return new Promise((resolve,reject) =>{
        if(userOneMeme){
            reject({
                name: 'User One is accepted',
                msg:'\nMsg : User one is implemented without any error\n'
            })
        }else if(userTwoMeme){
            reject({
                name:'User two is accepted',
                msg:'\nMsg : User two is implemented without any error\n'
            })
        }else{
            resolve('\nBoth are not implemented as the value is of loop become false\n')
        }
    })
}

watchUserPromise().then((msg) =>{
    console.log('\nSuccess' +msg)
}).catch((error) =>{
    console.log(error.name+' '+error.msg)
})