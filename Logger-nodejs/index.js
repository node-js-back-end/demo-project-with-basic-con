const log4js = require('log4js'); 
//-----------------------------info level---------------
log4js.configure({
    appenders: { 'file': { type: 'file', filename: 'logs/demo.log' } },
    categories: { default: { appenders: ['file'], level: 'error' } }
  });
//--------------------------error level--------------------
//   log4js.configure({
//     appenders: { 'file': { type: 'file', filename: 'logs/demo.log' } },
//     categories: { default: { appenders: ['file'], level: 'error' } }
//   });
//--------------------------debug level--------------------
//   log4js.configure({
//     appenders: { 'file': { type: 'file', filename: 'logs/demo.log' } },
//     categories: { default: { appenders: ['file'], level: 'debug' } }
//   });
const loggerinfo = log4js.getLogger('demo')

loggerinfo.trace("this is trace msg..")

loggerinfo.debug("this is debug msg..") 
////////////////Log levels debug which send detailed information.

loggerinfo.error("this is error msg..") 
////////////////////We want to log exceptions so we know when our application has an error.

loggerinfo.info("this is info msg..") 
////////////////////It’s also used for logging messages to other developers

loggerinfo.fatal("this is fatal msg..")

loggerinfo.warn("this is warn msg..") 
////////////We want to log exceptions so we know when our application has an error.

