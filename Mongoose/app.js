const mongoose = require("mongoose")


//connection created and creating a new database................
mongoose.connect("mongodb://localhost:27017/mongo-demo-first",{ useNewUrlParser: true }, { useUnifiedTopology: true })
.then(() => console.log("connection successfully..........."))
.catch((err) => console.log(err));

/// schema defining structure of documents-------------------------
const playlistSchema = new mongoose.Schema({
    name:{
        type : String,
        required : true,
        unique : true,
        uppercase:true       // built-in validators -----lowercase :true ----trim: true ----minLength : any---- maxLength:any
    },
    ctype :String,
    videos : Number,
    author:String,
    active : Boolean,
    date :{
        type : Date,
        default : Date.now
    }
})

////////////////---- model is a wrapper on mongoose schema. (##creating schema means creating documents)
////////////////-------schema defines the structure of the given document, validators while models provide an interface
////////////////--------- to the database for creating, querying, deleting, etc.,

const Playlist = new mongoose.model("Playlist",playlistSchema)

//create document / insert

const createDocument = async () =>{
    try{
        const jsPlaylist = new Playlist({
            name:"javascript",
            ctype :"front end",
            videos : 50,
            author:"abc",
            active : true,
            
        })
        const mongoPlaylist = new Playlist({
            name:"mongodb",
            ctype :"database",
            videos : 98,
            author:"abc",
            active : true,
            
        })
        const mongoosePlaylist = new Playlist({
            name:"            moNGOose           ",
            ctype :"database",
            videos : 80,
            author:"abc",
            active : true,
            
        })

        const reactPlaylist = new Playlist({
            name:"node js",
            ctype :"front end",
            videos : 80,
            author:"abc",
            active : true,
            
        })
        
       // const result = await reactPlaylist.save(); // save() return promises that is catch and try (for single )
        //console.log(result);
        const result = await Playlist.insertMany([mongoosePlaylist])
        
        //const result = await Playlist.insertMany([ jsPlaylist,mongoPlaylist,mongoosePlaylist,reactPlaylist ]); 
        console.log(result);
    
    }
    catch(err){
        console.log(err)
    }
    
}

createDocument();

const getDocument = async () =>{
    try{
        const result =await Playlist
       // .find({videos : {$lte:50} }) ///// lte=lt=gt=gte
       //.find({ctype: {$nin : ["database" , "front end"] } })  // in ~ nin
      // .find({$and: [ {author:"abc"} , {ctype:"database"} ] })  /////and=or=nor=not
      .find({author:"abc"}) 
      .select({name:1})
       .sort("name : 1") ////// name : 1 -- means asending order is followed and -1 means desending order
       //.countDocuments()   /////////// display count
        //.limit(1)
        console.log(result)
    }catch(err){
        console.log(err)
    }
  
}
//getDocument()  ///-----------display documents
