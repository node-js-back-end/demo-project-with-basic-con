const fs = require('fs')

fs.mkdir('yogita',(err) =>{
    console.log('folder created')
})

fs.writeFile("yogita/bio.txt","my name is yogita" , (err) =>{
    console.log("txt file created...")
})

fs.appendFile('yogita/bio.txt'," ............it is append part", (err) =>{
    console.log('data is appended')
})

fs.readFile("yogita/bio.txt","utf-8", (err , data ) =>{
    console.log(data)
})

fs.rename('yogita/bio.txt','yogita/mybio.txt', (err) =>{
    console.log('file is getting rename.....')
})

fs.unlink('yogita/mybio.txt',(err) =>{
    console.log('file deleted')
})

fs.rmdir('yogita',(err) =>{
    console.log('folder is deleted')
})