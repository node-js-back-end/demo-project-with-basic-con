const path = require('path');

var filename = path.basename('C:/Users/yogita.sawant/Desktop/Node-js/Node-modules-imp/Path-module/index.js');
console.log("1. "+filename);

var filename = path.dirname('C:/Users/yogita.sawant/Desktop/Node-js/Node-modules-imp/Path-module/index.js');
console.log("2. "+filename);

var filename = path.extname('C:/Users/yogita.sawant/Desktop/Node-js/Node-modules-imp/Path-module/index.js');
console.log("3. "+filename);

var filename = path.parse('C:/Users/yogita.sawant/Desktop/Node-js/Node-modules-imp/Path-module/index.js');
console.log("4. "+filename);

var filename = path.isAbsolute('C:/Users/yogita.sawant/Desktop/Node-js/Node-modules-imp/Path-module/index.js');
console.log("5. "+filename);