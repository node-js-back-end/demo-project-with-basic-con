const os = require('os')

const freeMemory = os.freemem()

console.log( `Freesapace of the memory :${freeMemory /1024/1024/1024}`)

console.log("Platform: " + os.platform());

console.log("Architecture: " + os.arch());

console.log("Endianness of the CPU: " + os.endianness());

console.log("Hostname of the operating system: " + os.hostname());

console.log("Array of the load averages: " + os.loadavg());

console.log("Uptime of the operating system in seconds: " + os.uptime());

console.log("Information about the current user: " + os.userInfo());

console.log("Name of the operating system: " + os.type());

console.log("Number of total memory of the system: " + os.totalmem());

console.log("Information about the operating system's platform: " + os.platform());

console.log("Network interfaces that has a network address: " + os.networkInterfaces());



