/////////////////////////////////---------EVENT EMITTER CLASS METHODS----------------
const events = require('events');
const process = require('process')

const fs = require('fs')

const eventEmitter = new events.EventEmitter();

///---------------adding the listener connection 1
const listner1 = function listner1() {
    console.log("listner 1 connection is executed");
}
////----------------adding the listener connection 2
const listner2 = function listner2() {
    console.log("listner 2 connection is executed");
}
///----------------subscribe---addlistener() add the event into the array like connection 1 is added on array
eventEmitter.addListener('connection', listner1);
//////////////on() add the event at the end of the given array so connection 2 is added after connection 1
eventEmitter.on('connection', listner2);
///--------------listenercount method counts the exact events from arguments ... by default their are 10 events
var eventListeners = require('events').EventEmitter.listenerCount
    (eventEmitter, 'connection');
console.log(eventListeners + " Listners listening to connection of the events");
///---------------emit() execute each of the connection in order with the supplied array arguments
eventEmitter.emit('connection');
///---------------removelistenner() removes the connection from supplied array arguments
eventEmitter.removeListener('connection', listner1);
console.log("Listner 1 connection will not listen after this");
eventEmitter.emit('connection');
eventListeners = require('events').EventEmitter.listenerCount(eventEmitter, 'connection');
console.log(eventListeners + " Listners listening to connection of the events");
////////////////-----------------------------------UNCAUGHT EXCEPTION METHOD------------------------

process.on('uncaughtException', (err, origin) => {
    fs.writeSync(
        process.stderr.fd,
        `Caught exception: ${err}\n` +
        `Exception origin: ${origin}`
    );
});

setTimeout(() => {
    console.log('\nThis will still run.');
}, 500);

nonexistentFunc();


